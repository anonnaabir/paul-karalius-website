import React, { Component } from "react";
import { graphql, StaticQuery } from 'gatsby';
import CarouselSlider from "../components/carousel";

import '../css/bootstrap.min.css';

class Gallery extends Component {
  constructor(props) {
    super(props);
  }

  slideJump = (index) => {
    console.log(index)
    this.props.jumptoSlide(index);
    console.log('Jump Done')
  }

    render(){
        return(
            <StaticQuery
            query={graphql`
            query SitePortfolioGallery {
              allWordpressWpPortfolio {
                edges {
                  node {
                    acf {
                      image {
                        localFile {
                          childImageSharp {
                            fluid(webpQuality: 80, maxWidth: 1458) {
                              srcWebp
                              src
                            }
                          }
                        }
                        media_details {
                          height
                          width
                        }
                      }
                    }
                  }
                }
              }
            }
              
            `}

            render={data => (
                  <div className="row flex image-works">
                        {data.allWordpressWpPortfolio.edges.map((portfolio,index) => {
                          {console.log("Image Width: " + portfolio.node.acf.image.media_details.width)}
                            return(
                                  <div className="col-lg-4 col-md-4 col-sm-6">
                                  <div className="thumbnail">
                                  <img
                                  src={portfolio.node.acf.image.localFile.childImageSharp.fluid.src}
                                  alt={portfolio.node.acf.project_name}
                                  className="gallery-image"
                                  index={index}
                                  onClick={() => this.slideJump(index)}
                                  style={{"pointer-events": "all"}}
                                   />

                                  <img
                                  src={portfolio.node.acf.image.localFile.childImageSharp.fluid.src}
                                  alt={portfolio.node.acf.project_name}
                                  className="gallery-image-mobile"
                                  index={index}
                                  onClick={() => this.slideJump(index)}
                                  style={{"pointer-events": "all"}}
                                   />
                                  </div>
                                  
                                  </div>
                            )})}
            </div>
            
            )}
              
            />
        )
    }
}

export default Gallery