import React, { Component } from "react";
import { graphql, StaticQuery } from 'gatsby';

import '../css/bootstrap.css';
import "react-bootstrap-carousel/dist/react-bootstrap-carousel.css";

import Slider from "react-slick";
import '../css/slick.css';

class CarouselSlider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      x: 0,
      y: 0
    };
    this.nextSlide = this.nextSlide.bind(this);
    this.previousSlide = this.previousSlide.bind(this);
    this.jumpSlide = this.jumpSlide.bind(this);
  }

  getOffset = e => {
    this.setState({
      x: e.clientX,
      y: e.clientY
    });

    if (this.state.x>800) {
      console.log('Class Right');
    }

    else {
      console.log('Cursor Left');
    }
  }



    disableMenu = (e) => {
    e.preventDefault();
  }

  moveSlide = () => {
    const {x} = this.state;

    if (x>800) {
      this.nextSlide()
    }

    else {
      this.previousSlide()
    }

  }

  nextSlide = () => {
    console.log("Call Done");
    this.slider.slickNext();
  }

  previousSlide = () => {
    this.slider.slickPrev();
  }

  jumpSlide = (e) => {
    console.log('Done')
    this.slider.slickGoTo(e,true);
  }

    render(){
        return(
            <StaticQuery
            query={graphql`
            query SitePortfolio {
              allWordpressWpPortfolio {
                edges {
                  node {
                    title
                    slug
                    acf {
                      project_name
                      image {
                        localFile {
                          childImageSharp {
                            fluid (maxWidth:1458){
                              src
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }

            
          }
              
            `}

            render={data => (
                          <div>
                            <div className="image-cursor-left" onClick={this.previousSlide}></div>
                          <Slider
                          ref={c => (this.slider = c)}
                          dots={false}
                          arrows={false}
                          fade={true}
                          infinite={true}
                          speed={500}
                          slidesToShow={1}
                          slidesToScroll= {1}
                          newSlide={this.nextSlide}
                          className="main-carousel container-fluid"
                          >
                        {data.allWordpressWpPortfolio.edges.map(carousel => {
                            return(
                                <div>
                                <img
                                id="main-carousel-image"
                                className="carousel-image"
                                src={carousel.node.acf.image.localFile.childImageSharp.fluid.src}
                                alt={carousel.node.acf.project_name}
                                onMouseMove={this.getOffset}
                                onClick={this.moveSlide}
                                onContextMenu={this.disableMenu}
                                    />
                                    </div>
                            )})}
                            </Slider>
                            <div className="image-cursor-right" onClick={this.nextSlide}></div>
              </div>
            )}
              
            />
        )
    }
}

export default CarouselSlider