//import { Link } from "gatsby"
//import PropTypes from "prop-types"

import React, { Component } from "react";
// import { graphql, StaticQuery } from 'gatsby';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTwitter,faInstagram } from '@fortawesome/free-brands-svg-icons'
import '../css/bootstrap.min.css';
import '../css/style.css';

class Footer extends Component {

    render() {
      return (
        <footer class="footer fixed-bottom">
        <div class="container-fluid pkg-footer">
            <div class="d-flex justify-content-around">
            <p>Copyright © 2021 Paul Karalius</p>
            <div class="footer-icons"> 
            <a href="https://twitter.com/paulkaralius"><FontAwesomeIcon 
            icon={faTwitter}
            className="pkg-footer-icon" /></a>
            <a href="https://www.instagram.com/paulkaralius/"><FontAwesomeIcon
            icon={faInstagram}
            className="pkg-footer-icon"
            /></a>
            </div>
            </div>
  </div>
</footer>
        );
    }
  }

 
  export default Footer;
