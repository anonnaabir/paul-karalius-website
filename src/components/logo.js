//import { Link } from "gatsby"
//import PropTypes from "prop-types"

import React, { Component } from "react";
import { graphql, StaticQuery } from 'gatsby';

class Logo extends Component {
    render(){
        return(
            <StaticQuery
            query={graphql`
        query SiteLogo {
            file(relativePath: {eq: "desktop_logo.png"}) {
                childImageSharp {
                  fluid (webpQuality: 80, maxWidth:150) {
                    srcWebp
                    src
                  }
                }
              }
            }
        `}
            
        render={data => (
            <div>
            <a href="/"><img src={data.file.childImageSharp.fluid.src} className="site-logo"/></a>
            </div>
        )}

        />
        )
    }
}

export default Logo