//import { Link } from "gatsby"
//import PropTypes from "prop-types"

import React, { Component } from "react";
import { graphql, StaticQuery } from 'gatsby';
import MobileLogo from "./mobile-logo";
import ProjectList from "./project-list.js";
import '../css/bootstrap.min.css';
import '../css/style.css';

import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'


class MobileHeader extends Component {
  
  openNav = () => {
    document.getElementById("mobile-menu-id").style.width = "100%";
  }

  closeNav = () => {
    document.getElementById("mobile-menu-id").style.width = "0";
    document.getElementById("mobile-menu-container").style.marginLeft= "0";
  } 

    showDropdown = () => {
      document.getElementById("show-dropdown").style.display = "block";
      console.log('Alhamdulillah. It works.')
    }

    render() {
      return (
        <StaticQuery 
        query={graphql`
        query MobileMenu {
          allWordpressWpMenu {
            nodes {
              title
              url
            }
          },

        }

        `}
        render={data => (
            <div className="mobile-header-background">
                <div className="mobile-logo">
                <MobileLogo />
                </div>
                <div className="mobile-icon" >
                <p onClick={this.openNav}>MENU</p>
                </div>

                <div id="mobile-menu-id" className="mobile-menu">
                
                <div id="mobile-menu-container">
                
                {data.allWordpressWpMenu.nodes.map(menus => {
                return(
                <a href={menus.url}>{menus.title}</a>
                )})}
                <br /><br />
                <a href="#" onClick={this.closeNav}>Close Menu</a>
                </div>
                
                </div>
                
            </div>
        )}
        />

        );
    }
  }

 
  export default MobileHeader;
