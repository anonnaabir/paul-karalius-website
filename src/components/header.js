//import { Link } from "gatsby"
//import PropTypes from "prop-types"

import React, { Component } from "react";
import { graphql, StaticQuery } from 'gatsby';
import Logo from "../components/logo";
import '../css/bootstrap.min.css';
import '../css/style.css';

class Header extends Component {

    render() {
      return (
        <StaticQuery 
        query={graphql`
        query SiteMenu {
          allWordpressWpMenu {
            nodes {
              title
              url
            }
          },

        }

        `}
        render={data => (
          <div className="container sticky-top">
            {/* <a href="/">
            <div className="desktop-logo">
            <Logo />
            </div>
            </a> */}

          <div className="desktop-menu">
          <Logo />
          {data.allWordpressWpMenu.nodes.map(menus => {
              return(
                <a href={menus.url}>{menus.title}</a>
              )})}
          </div>
          </div>
            
        )}
        />

        );
    }
  }

 
  export default Header;
