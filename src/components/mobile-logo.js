//import { Link } from "gatsby"
//import PropTypes from "prop-types"

import React, { Component } from "react";
import { graphql, StaticQuery } from 'gatsby';

class MobileLogo extends Component {
    render(){
        return(
            <StaticQuery
            query={graphql`
        query SiteLogoText {
            file(relativePath: {eq: "mobile-logo.png"}) {
                childImageSharp {
                  fluid (webpQuality: 80, maxWidth:70) {
                    srcWebp
                    src
                  }
                }
              }
            }
        `}
            
        render={data => (
            <div>
            <img src={data.file.childImageSharp.fluid.src} />
            </div>
        )}

        />
        )
    }
}

export default MobileLogo