import React, {Component} from "react"
import { graphql, StaticQuery } from 'gatsby';

//import Layout from "../components/layout"
//import Image from "../components/image"
import SEO from "../components/seo"

import Header from "../components/header";
import Footer from "../components/footer";
import MobileHeader from "../components/mobile-header";
import Gallery from "../components/gallery";
import Slider from "react-slick";
import '../css/slick.css';
import '../css/bootstrap.min.css';

  

class Projects extends Component {
    constructor(props) {
      super(props)
      this.child = React.createRef();

      this.state = {
        projectName: ""
      }
    }
    
  
  changeView = () => {
    console.log("Alhamdulillah, view changed")
    document.getElementById("projects-footer-actions").style.display = "none";
    document.getElementById("portfolio-carousel").style.display = "none";
    document.getElementById("projects-gallery").style.display = "block";
  }

  previousSlide = () => {
    this.slider.slickPrev();
  }

  nextSlide = () => {
    this.slider.slickNext();
  }

  jumptoSlide = (index,projectname) => {
    this.slider.slickGoTo(index);
    document.getElementById("portfolio-footer-actions").style.display = "block";
    document.getElementById("portfolio-carousel").style.display = "block";
    document.getElementById("portfolio-gallery").style.display = "none";
    this.setState({projectName: projectname})
  }

  disableMenu = (e) => {
    e.preventDefault();
  }

  render() {
    return (
      <StaticQuery
            query={graphql`
            query ProjectsPage {
                allWordpressWpProjects {
                  edges {
                    node {
                      title
                      slug
                      acf {
                        project_name
                        main_image {
                          localFile {
                            childImageSharp {
                              fluid (webpQuality: 80, maxWidth:1458) {
                                src
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            `}

            render={data => (
              <div>
              <div className="container-fluid">
              <div className="row flex">
              <div className="col-md-2">
              <SEO
              title="Projects"
              />
              <Header />
              <div className="mobile-container">
                    <MobileHeader />
                </div>
              <div className="footer-controls" id="projects-footer-actions">
              <div className="col-md-2">
              <p>{this.state.projectName}</p>
                <div className="slide-controls">
              <a href="#" onClick={this.previousSlide}>Prev</a>
              <a href="#" onClick={this.nextSlide}>Next</a>
                </div>
              <a href="#" onClick={this.changeView}>Show Thumbnails</a>
              </div>
              </div>
              </div>

              {/* Gallery Start */}

              <div id="projects-gallery" className="col-md-10">
              <div className="row flex image-works pkg-wrapper">
                        {data.allWordpressWpProjects.edges.map((projects,index) => {
                            return(
                                  <div className="col-lg-4 col-md-4 col-sm-6">
                                  <div className="thumbnail">
                                    <div className="gallery-image">
                                  <a href={projects.node.slug}>
                                    <div class="img-container">
                                  <img
                                  src={projects.node.acf.main_image.localFile.childImageSharp.fluid.src}
                                  alt={projects.node.acf.project_name}
                                  index={index}
                                  onClick={() => this.jumptoSlide(index,projects.node.acf.project_name)}
                                  style={{"pointer-events": "all"}}
                                   />
                                   <div class="overlay">
                                   <p 
                                   style={{"pointer-events": "all"}}
                                   id="projects-title">
                                    {projects.node.acf.project_name}</p>
                                    </div>
                                    </div>
                                   </a>
                                   </div>

                                   <div className="gallery-image-mobile">
                                   <a href={projects.node.slug}>
                                   <div class="img-container"> 
                                  <img
                                  src={projects.node.acf.main_image.localFile.childImageSharp.fluid.src}
                                  alt={projects.node.acf.project_name}
                                  index={index}
                                  onClick={() => this.jumptoSlide(index,projects.node.acf.project_name)}
                                  style={{"pointer-events": "all"}}
                                   />
                                    <div class="overlay-mobile">
                                   <p id="projects-title">{projects.node.acf.project_name}</p>
                                   </div>
                                   </div>
                                   </a>
                                   </div>

                                  </div>
                                  
                                  </div>
                            )})}
            </div>
              </div>

                {/* Gallery End */}

                                 
              </div>
              </div>
              <Footer />
              </div>
)}
  
/>

      );
  }
}

  export default Projects;