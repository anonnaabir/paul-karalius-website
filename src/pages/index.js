import React, {Component} from "react"
import { graphql, StaticQuery } from 'gatsby';

//import Layout from "../components/layout"
//import Image from "../components/image"
import SEO from "../components/seo";

import Header from "../components/header";
import Footer from "../components/footer";
import MobileHeader from "../components/mobile-header";
import Gallery from "../components/gallery";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft, faArrowRight } from '@fortawesome/free-solid-svg-icons';

import Slider from "react-slick";
import '../css/slick.css';
import '../css/bootstrap.min.css';

  

class IndexPage extends Component {
    constructor(props) {
      super(props)
      this.child = React.createRef();

      this.state = {
        projectName: ""
      }
    }
    
  
  changeView = () => {
    console.log("Alhamdulillah, view changed")
    document.getElementById("portfolio-footer-actions").style.display = "none";
    document.getElementById("portfolio-carousel").style.display = "none";
    document.getElementById("portfolio-gallery").style.display = "block";
  }

  previousSlide = () => {
    this.slider.slickPrev();
  }

  nextSlide = () => {
    this.slider.slickNext();
  }

  jumptoSlide = (index,projectname) => {
    this.slider.slickGoTo(index);
    document.getElementById("portfolio-footer-actions").style.display = "block";
    document.getElementById("portfolio-carousel").style.display = "block";
    document.getElementById("portfolio-gallery").style.display = "none";
    this.setState({projectName: projectname})
  }

  disableMenu = (e) => {
    e.preventDefault();
  }

  render() {
    return (
      <StaticQuery
            query={graphql`
            query PortfolioPage {
                allWordpressWpPortfolio {
                  edges {
                    node {
                      title
                      acf {
                        project_name
                        image {
                          localFile {
                            childImageSharp {
                              fluid (webpQuality: 80, maxWidth:1458) {
                                src
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            `}

            render={data => (
              <div>
              <div className="container-fluid">
              <div className="row flex pkg-wrapper">
              {/* <div className="col-md-2"> */}
              <div className="col-md-2">
              <SEO
              title="Portfolio"
              />
              <Header />
              <div className="mobile-container">
                    <MobileHeader />
                </div>
              <div className="footer-controls flex-column" id="portfolio-footer-actions">
              {/* <div className="col-md-2"> */}
              {/* <p id="portfolio-project-name">{this.state.projectName}</p> */}
                <div className="slide-controls">
              <a href="#" onClick={this.previousSlide}><FontAwesomeIcon icon={faArrowLeft} /></a>
              <a href="#" onClick={this.nextSlide}><FontAwesomeIcon icon={faArrowRight} /></a>
                </div>
              <a href="#" onClick={this.changeView}>Show Thumbnails</a>
              </div>
              {/* </div> */}
              </div>

              {/* Carousel Start */}
              <div id="portfolio-carousel" className="align-items-center col-md-10">
              <div className="image-cursor-left" onClick={this.previousSlide}></div>
                          <Slider
                          ref={c => (this.slider = c)}
                          dots={false}
                          arrows={false}
                          fade={true}
                          infinite={true}
                          speed={500}
                          slidesToShow={1}
                          slidesToScroll= {1}
                          newSlide={this.nextSlide}
                          className="main-carousel container-fluid"
                          >
                        {data.allWordpressWpPortfolio.edges.map(portfolio => {
                            return(
                                <div>
                                <img
                                id="main-carousel-image"
                                // className="img-fluid"
                                className="carousel-image"
                                src={portfolio.node.acf.image.localFile.childImageSharp.fluid.src}
                                alt={portfolio.node.acf.project_name}
                                onContextMenu={this.disableMenu}
                                    />
                                    </div>
                            )})}
                            </Slider>
                            <div className="image-cursor-right" onClick={this.nextSlide}></div>
              </div>
              {/* Carousel End */}

              {/* Gallery Start */}

              <div id="portfolio-gallery" className="col-md-10">
              <div className="row flex image-works">
                        {data.allWordpressWpPortfolio.edges.map((portfolio,index) => {
                            return(
                                  <div className="col-lg-4 col-md-4 col-sm-6">
                                  <div className="thumbnail">
                                  <img
                                  src={portfolio.node.acf.image.localFile.childImageSharp.fluid.src}
                                  alt={portfolio.node.acf.project_name}
                                  className="gallery-image"
                                  width="100%"
                                  height="330"
                                  index={index}
                                  onClick={() => this.jumptoSlide(index,portfolio.node.acf.project_name)}
                                  style={{"pointer-events": "all"}}
                                   />
                                  </div>
                                  
                                  </div>
                            )})}
            </div>
              </div>
              <div className="col-lg-10">
              <div className="mobile-container">
                    {/* <MobileHeader /> */}
                    <Gallery className="main-gallery" />
                </div>
                </div>

                {/* Gallery End */}


              </div>
              </div>
              <Footer />
              </div>
)}
  
/>

      );
  }
}

  export default IndexPage;