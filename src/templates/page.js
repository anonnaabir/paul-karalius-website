import React, { Component } from "react";
import { graphql } from 'gatsby';
import stripHtml from "string-strip-html";


//import Layout from "../components/layout"
//import Image from "../components/image"
import SEO from "../components/seo"

import Header from "../components/header";
import Footer from "../components/footer";
import MobileHeader from "../components/mobile-header";

import '../css/bootstrap.min.css';


              const Page = ({data}) => {
                return (
                  <div>
                  <div className="container-fluid">
                            <div className="row flex">
                            <div className="col-md-2">
                            <SEO
                        title={data.wordpressPage.title}
                            />
                            <Header />
                            <div className="mobile-container">
                                <MobileHeader  />
                            </div>
                            </div>
                            <div className="col-lg-6">
                            <div className="page-content">
                              <h1>{data.wordpressPage.title}</h1>
                              <br /> 
                              <div dangerouslySetInnerHTML={{ __html: data.wordpressPage.content }} />
                            </div>
                            </div>
                            </div>
                            </div>
                            <Footer />
                            </div>
                             
                )
              }


      export const query = graphql`
      query Page ($slug: String) {
        wordpressPage(slug: {eq: $slug}) {
            title
            content
            slug
          }
        }
      `

      export default Page;