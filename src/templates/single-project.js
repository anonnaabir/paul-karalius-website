import React, { useEffect,useRef} from "react";
import { graphql } from 'gatsby';

import Slider from "react-slick";
import '../css/slick.css';
import SEO from "../components/seo";
import Header from "../components/header";
import Footer from "../components/footer";
import MobileHeader from "../components/mobile-header";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft, faArrowRight } from '@fortawesome/free-solid-svg-icons';

import '../css/bootstrap.min.css';

        
      const nextSlide = () => {
        document.getElementById("carousel-next").click();
      }
    
      const previousSlide = () => {
        document.getElementById("carousel-previous").click();
      }


      

    const SingleProject = ({data}) => {
      useEffect(() => {
        // Update the document title using the browser API
        document.getElementsByClassName("slick-prev")[0].setAttribute("id", "carousel-previous");
        document.getElementsByClassName("slick-next")[0].setAttribute("id", "carousel-next");
        document.getElementById("carousel-previous").style.display = "none";
        document.getElementById("carousel-next").style.display = "none";
        // document.getElementById("single-project-controls").style.display = "block";
      });

      const changeView = () => {
        console.log("Alhamdulillah, view changed")
        document.getElementById("single-project-footer-actions").style.display = "none";
        document.getElementById("single-project-carousel").style.display = "none";
        document.getElementById("single-project-gallery").style.display = "block";
        document.getElementById("thumbnail-toggle").style.display = "none";
      }


      // const jumptoSlide = () => {
      //   console.log('Alhamdulillah. Dugdugi');
      // }

      
      return (
        <div>
        <div class="container-fluid">
          <div class="row flex">
          <div class="col-md-2">
          <SEO
              title={data.wordpressWpProjects.acf.project_name}
              />
          <Header />
            <div className="mobile-container">
                <MobileHeader />
            </div>
          <div className="footer-controls flex-column" id="single-project-footer-actions">
          {/* <div className="col-md-2"> */}
          <p id="portfolio-project-name">{data.wordpressWpProjects.acf.project_name}</p>
          <div className="slide-controls">
              <a href="#" onClick={previousSlide}><FontAwesomeIcon icon={faArrowLeft} /></a>
              <a href="#" onClick={nextSlide}><FontAwesomeIcon icon={faArrowRight} /></a>
                </div>
                <a id="thumbnail-toggle" href="#" onClick={changeView}>Show Thumbnails</a>
            {/* </div> */}
            </div>

          </div>

          {/* Carousel Style Start */}

          <div id="single-project-carousel" className="col-lg-10">
                            <div className="image-cursor-left" onClick={previousSlide}></div>
                          <Slider
                          dots={false}
                          arrows={true}
                          fade={true}
                          infinite={true}
                          speed={500}
                          slidesToShow={1}
                          slidesToScroll= {1}
                          className="main-carousel container-fluid"
                          >
                            <img
                                id="main-carousel-image"
                                className="carousel-image"
                                src={data.wordpressWpProjects.acf.main_image.localFile.childImageSharp.fluid.src}
                                    />
                        {data.wordpressWpProjects.acf.all_images.map(images => {
                            return(
                                <div>
                                <img
                                id="main-carousel-image"
                                className="carousel-image"
                                src={images.localFile.childImageSharp.fluid.src}
                                    />
                                    </div>
                            )})}
                            {/* <img src={data.wordpressWpProject.acf.main_image.localFile.childImageSharp.fluid.src} />
                            {console.log(data.wordpressWpProject.acf.main_image.localFile.childImageSharp.fluid.src)}
                            {data.wordpressWpProject.acf.all_images.map(images => {
                                console.log(images.localFile.childImageSharp.fluid.src)
                            })} */}
                            </Slider>
                            <div className="image-cursor-right" onClick={nextSlide}></div>
              </div>

          {/* Carousel Style End */}


          {/* Gallery Start */}

          <div id="single-project-gallery" className="col-md-10 main-gallery">
              <div className="row flex image-works">
                        {data.wordpressWpProjects.acf.all_images.map((images,index) => {
                            return(
                                  <div className="col-lg-4 col-md-4 col-sm-6">
                                  <a
                                  href="#"
                                  index={images.localFile.childImageSharp.id}
                                  // onClick={() => jumptoSlide(index,images.localFile.childImageSharp.id)}
                                  onClick={() =>  {
                                    console.log('Alhamdulillah. Inline function worked.')
                                    document.getElementById("single-project-carousel").style.display = "block";
                                    document.getElementById("single-project-gallery").style.display = "none";
                                    document.getElementById("thumbnail-toggle").style.display = "block";
                                    document.getElementById("single-project-footer-actions").style.display = "block";
                                  }}
                                  >
                                  <div className="thumbnail">
                                  <img
                                  src={images.localFile.childImageSharp.fluid.src}
                                  className="gallery-image"
                                  
                                  />

                                  <img
                                  src={images.localFile.childImageSharp.fluid.src}
                                  className="gallery-image-mobile"
                                   />

                                  </div>
                                  </a>
                                  </div>
                            )})}
            </div>
              </div>

                {/* Gallery End */}
          </div>
          </div>
          <Footer />
          </div>
      )
    }

      export const query = graphql`
      query SingleProject ($slug: String) {
          wordpressWpProjects (path: {eq: $slug}) {
            title
            slug
            acf {
              project_name
              main_image {
                localFile {
                  childImageSharp {
                    fluid (webpQuality: 80, maxWidth:1458) {
                      src
                    }
                  }
                }
              }
              all_images {
                localFile {
                  childImageSharp {
                    fluid (webpQuality: 80, maxWidth:1458) {
                      src
                    }
                    id
                  }
                }
              }
            }
          }

      }
      `

export default SingleProject;